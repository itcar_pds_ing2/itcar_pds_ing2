package com.itcar.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such order")
public class ResourceNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8790211652911971729L;
	
	public ResourceNotFoundException(long id){
		super("There is no hit with id: "+id);
	}
	
	public ResourceNotFoundException(){
		super("The ressource was don't found");
	}
}
