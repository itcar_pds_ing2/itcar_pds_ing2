package com.itcar.exception;

import lombok.Data;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ExceptionHandlerAdvice {

	@ExceptionHandler
	@ResponseBody
	ExceptionRepresentation handle(Exception exception){
		return new ExceptionRepresentation(exception.getLocalizedMessage());
	}
	
	@Data
	class ExceptionRepresentation{
		String message;

		public ExceptionRepresentation(String message) {
			this.message = message;
		}
		
	}
}
