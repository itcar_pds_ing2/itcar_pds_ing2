package com.itcar.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.itcar.domain.User;
import com.itcar.exception.ResourceNotFoundException;
import com.itcar.repository.UserRepository;

/**
 * Created by b27661 on 07/08/2017.
 */

@Controller
@RequestMapping("/user")
public class UserController {
	
    @Autowired
    private UserRepository userRepository;
    private Object User;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public User getUser(@PathVariable("id") Long id){
    	
    	User response = userRepository.findOne(id);
        if (response == null) throw new ResourceNotFoundException(id);
    	
        return response;
    }

    @RequestMapping(value="/all-user", method = RequestMethod.GET)
    @ResponseBody
    public List<User> getUser() {
    	
    	List<User> list = new ArrayList<User>();
    	list=userRepository.findAll();
        if (list == null) throw new ResourceNotFoundException();
        return list;
    }
    
}
