package com.itcar.repository;

import com.itcar.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by b27661 on 07/08/2017.
 */

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
}
