package com.itcar.domain;

/**
 * Created by b27661 on 07/08/2017.
 */

import javax.persistence.*;

import java.io.Serializable;

import lombok.Data;

@Entity
@Table(name = "user_test")
@Data
public class User implements Serializable {
	
    @Id
//    @GeneratedValue(generator="seqGen")
//    @SequenceGenerator(name="seqGen",sequenceName="user_test_id_seq", allocationSize=1)
    @Column(name = "id")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "username")
    private String username;
    
    public User(){
    	
    }
    
    public User(long id, String firstName, String lastName) {
    	this.id = id;
    	this.firstName = firstName;
    	this.lastName = lastName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
    
    
}
